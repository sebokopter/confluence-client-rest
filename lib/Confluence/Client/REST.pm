package Confluence::Client::REST;

use 5.006;
use strict;
use warnings;
use utf8;

use Carp qw(confess);
use Cwd qw(getcwd);
use Data::Dumper;
use File::Basename qw(basename dirname);
use File::Util qw(escape_filename);
use JSON;
use MIME::Base64 qw(encode_base64);
use Moose;
use REST::Client;
use URI::Encode qw(uri_encode);

=head1 NAME

Confluence::Client::REST - Client for the Atlassian Confluence wiki, based on their REST-API

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';

=head1 SYNOPSIS

This module let you interact with any Confluence wiki via the REST API.

    use Confluence::Client::REST;

    my $confluence = Confluence::Client::REST->new({url => 'https://foo.bar.example.com', username => 'foo', password => 'bar'});
    my $confluenceDebug = Confluence::Client::REST->new({url => 'https://foo.bar.example.com', username => 'foo', password => 'bar', debug => 1});

    $confluence->getContent();
    $confluence->getContent($id); 

    $confluence->createContent({title => 'foo', space => {key => '~bar'}, body => { storage => { value => "<p>baz</p>" }}});
    $confluence->createContent({type => 'page', title => 'foo', space => {key => '~bar'}, body => { storage => { value => "baz" }, representation => 'storage'}});
    $confluence->createContent({type => 'page', title => 'foo', ancestors => [{id => 456}], space => {key => '~bar'}, body => { storage => { value => "<p>baz</p>" }, representation => 'storage'}});
    $confluence->createContent({title => 'foo', spaceKey => '~bar'});

    $confluence->updateContent({id => $id, type => 'page', title => 'new page', space => { key => 'TST' }, body => { storage => {value => '<p>foo bar</p>'}, representation => 'storage' }, version => { number => 2 } });
    $confluence->updateContent({id => $id, type => 'page', title => 'new page', spaceKey => 'TST', storageValue => '<p>foo bar</p>', storageRepresentation => 'storage' });
    $confluence->updatePage({id => $id, title => 'new page', spaceKey => 'TST' , storageValue => '<p>foo bar</p>', });

    $confluence->uploadAttachment($id, $filename);

    $confluence->deleteContent($id);

    $confluence->convertView('<ac:structured-macro ac:name=\"cheese\" />', 'storage');
    # with page $id context
    $confluence->convertView('<ac:structured-macro ac:name=\"cheese\" />', 'storage', $id);
    $confluence->convertStorage('|| header1 || header2 ||', 'wiki');
    # with page $id context
    $confluence->convertStorage('|| header1 || header2 ||', 'wiki', $id);

=head1 CONSTANTS

=head2 REST_API_PATH

=cut

use constant REST_API_PATH => '/rest/api';

=head1 ATTRIBUTES

=head2 url 

=cut

has 'url' => (is => 'rw', isa => 'Str', required => 1);

=head2 username

=cut

has 'username' => (is => 'rw', isa => 'Str', required => 1);

=head2 password 

=cut

has 'password' => (is => 'rw', isa => 'Str', required => 1);

=head2 debug

=cut 

has 'debug' => (is => 'rw', isa => 'Bool', required => 0, default => undef);

=head2 restClient

=cut

has 'restClient' => (is => 'ro', isa => 'REST::Client', required => 1);

=head2 lwpUserAgent

=cut

has 'lwpUserAgent' => (is => 'ro', isa => 'LWP::UserAgent', required => 1);

=head2 lwpUserAgentSslOpts

=cut

has 'lwpUserAgentSslOpts' => (is => 'ro', isa => 'HashRef', required => 0);

around BUILDARGS => sub {
        my $orig = shift;
        my $class = shift;

        # get args
        my %args = ref $_[0] ? %{shift()} : @_;
        # verify attributes were given to args
        die("No 'url' parameter given.") unless defined $args{url};
        die("No 'username' parameter given.") unless defined $args{username};
        die("No 'password' parameter given.") unless defined $args{password};

        # init REST::Client instance
        my $restClient = REST::Client->new( { host => $args{url} } );
        $restClient->addHeader( 'Accept', 'application/json' );
        $restClient->addHeader( 'Content-Type', 'application/json' );
        my $basicAuth = encode_base64($args{username}.':'.$args{password});
        chomp $basicAuth;
        $restClient->addHeader( 'Authorization', 'Basic '.$basicAuth );

        # inject REST::Client instance
        $args{restClient} = $restClient;

        # init LWP::UserAgent
        my $lwpUserAgent = LWP::UserAgent->new();
        $lwpUserAgent->ssl_opts( $args{lwpUserAgentSslOpts} );

        # inject REST::Client instance
        $args{lwpUserAgent} = $lwpUserAgent;

        # put args back
        return $class->$orig( %args );
    };

=head1 METHODS

=head2 getContent()

TODO

=cut

sub getContent {
    my $self = shift;
    my $pageId = shift;

    warn( "Call getContent() with parameter:\n ".Dumper( $pageId ) ) if $self->{debug};

    confess("getContent() expects a numeric 'pageId' parameter.")
        if defined $pageId && $pageId !~ /^\d+$/;

    return $self->_getContent( $pageId );
}

sub getChildren {
    my $self = shift;
    my $pageId = shift;

    warn( "Call getChildren() with parameter:\n ".Dumper( $pageId ) ) if $self->{debug};

    confess("getChildren() expects a numeric 'pageId' parameter.")
        if defined $pageId && $pageId !~ /^\d+$/;

    return $self->_getContent( $pageId, { expand => qw(children.page) } );
}

sub _getContent {
    my $self = shift;
    my $pageId = shift;
    my $paramHash = shift;
    my $queryPath = REST_API_PATH."/content";

    warn( "Call _getContent() with parameters:\n ".Dumper({pageId => $pageId, hash => $paramHash}) ) if $self->{debug};

    if (defined $pageId) {
        if ($pageId =~ /^\d+$/) {
            $queryPath .= "/$pageId";
        } else {
            confess("_getContent() expects an optional id as argument but none was given.\n")
        }
    }

    confess("_getContent() expects an optional hashref as argument, but none was given.\n")
        if defined $paramHash && ref $paramHash ne 'HASH';
    # default parameter if none given
    $paramHash->{start} //= 0;
    $paramHash->{limit} //= 25;

    $queryPath .= $self->_convertHashToParamUrl( $paramHash );

    my $output = $self->_query( $queryPath );

    $output = $self->_resolveAllNextUrls( $output );

    warn( "_getContent() returns:\n ".Dumper($output) ) if $self->{debug};
    return $output;
}

sub _query {
    my $self = shift;
    my $queryPath = shift;

    warn( "Request:\n ".Dumper($queryPath) ) if $self->{debug};

    $self->{restClient}->GET( $queryPath );

    my $responseCode = $self->{restClient}->responseCode();
    my $responseContent = $self->{restClient}->responseContent();

    warn( "Response:\n ".Dumper($responseContent) ) if $self->{debug};

    confess('Request returned unsuccessfully with response code '.$responseCode) unless $responseCode < 300;

    my $output = decode_json($responseContent);
    warn( "JSON decoded Response:\n ".Dumper($output) ) if $self->{debug};

    return $output;
}

sub _resolveAllNextUrls {
    my $self = shift;
    my $hash = shift;
    my $newHash = $hash;

    warn( "Call _resolveAllNextUrls() with parameter:\n ".Dumper($newHash) ) if $self->{debug};
    if (ref $newHash eq 'HASH') {
        if (my $links = $newHash->{_links}) {
            if ($links->{self} && $links->{next}) {
                warn("_resolveAllNextUrls() found pagination. I.e. '_links' key is available:\n ".Dumper($newHash->{_links}))
                    if $self->{debug};
                $newHash->{_links}{self} =~ s#^https?://.+?(?=/)##;
                warn("Start getting all page results.\n") if $self->{debug};
                $newHash = $self->_getNextUrlsResult( $newHash->{_links}{self} );
            }
        }
        warn( "Iterate over\n ".Dumper($newHash) ) if $self->{debug};
        while ( my ($key, $value) = each %$newHash ) {
            $newHash->{$key} = $self->_resolveAllNextUrls( $value );
        }
    }
    return $newHash;
}

sub _getNextUrlsResult {
    my $self = shift;
    my $queryPath = shift;

    warn( "_getNextUrlsResult('$queryPath') called\n") if $self->{debug};

    my $output = $self->_query( $queryPath );

    warn( "JSON decoded Response:\n ".Dumper($output) ) if $self->{debug};

    if (defined $output->{_links}{next}) {
        my $recursionOutput = $self->_getNextUrlsResult( $output->{_links}{next} );
        my $outputResults = [ @{$output->{results}}, @{$recursionOutput->{results}} ];
        $output = {
            results => $outputResults,
            size    => $output->{size} + $recursionOutput->{size},
            start   => $output->{start},
            limit   => $output->{size} + $recursionOutput->{size},
            _links  => {
                self    => $recursionOutput->{_links}{self},
                base    => $recursionOutput->{_links}{base},
                context => $recursionOutput->{_links}{context},
            },
        };
    }
    warn( "_getNextUrlsResult() returns:\n ".Dumper($output) ) if $self->{debug};

    return $output;
}

=head2 createContent

TODO

=cut

sub createContent {
    my $self = shift;
    my $contentDataHashref = shift;

    # replace convenience alternative key 'spaceKey' (equals ->{space}->{key})
    if (defined $contentDataHashref->{spaceKey}) {
        $contentDataHashref->{space}{key} = $contentDataHashref->{spaceKey};
        delete $contentDataHashref->{spaceKey};
    };
    # replace convenience alternative key 'storageValue' (equals ->{body}->{storage}->{value})
    if (defined $contentDataHashref->{storageValue}) {
        $contentDataHashref->{body}{storage}{value} = $contentDataHashref->{storageValue};
        delete $contentDataHashref->{storageValue};
    };
    # replace convenience alternative key 'storageRepresentation' (equals ->{body}->{storage}->{representation})
    if (defined $contentDataHashref->{storageRepresentation}) {
        $contentDataHashref->{body}{storage}{representation} = $contentDataHashref->{storageRepresentation};
        delete $contentDataHashref->{storageRepresentation};
    };

    # set default for some necessary values
    $contentDataHashref->{body}{storage}{representation} //= 'storage';
    $contentDataHashref->{type} //= 'page';

    my $jsonContentData = encode_json($contentDataHashref);
    $self->{restClient}->POST( 'rest/api/content', $jsonContentData );
    warn(Dumper($self->{restClient})) if $self->{debug};
    confess('createContent() returned unsuccessfully') unless $self->{restClient}->responseCode < 300;
    return decode_json($self->{restClient}->responseContent());
}

=head2 updateContent

TODO

=cut

sub updateContent {
    my $self = shift;
    my $contentDataHashref = shift;
    my $id;

    confess("hashref expected, but '$contentDataHashref' was given") unless ref $contentDataHashref eq 'HASH';
    if (defined $contentDataHashref->{id}) {
        $id = $contentDataHashref->{id};
    } else {
        confess('updateContent has to be called with content ID');
    }

    $contentDataHashref->{version}{number} //= ${$self->getContent( $id,
        { expand => qw(version) } )}{version}{number} + 1;

    #TODO: duplicate code (see createContent), move it to own private method
    # replace convenience alternative key 'spaceKey' (equals ->{space}->{key})
    if (defined $contentDataHashref->{spaceKey}) {
        $contentDataHashref->{space}{key} = $contentDataHashref->{spaceKey};
        delete $contentDataHashref->{spaceKey};
    };
    # replace convenience alternative key 'storageValue' (equals ->{body}->{storage}->{value})
    if (defined $contentDataHashref->{storageValue}) {
        $contentDataHashref->{body}{storage}{value} = $contentDataHashref->{storageValue};
        delete $contentDataHashref->{storageValue};
    };
    # replace convenience alternative key 'storageRepresentation' (equals ->{body}->{storage}->{representation})
    if (defined $contentDataHashref->{storageRepresentation}) {
        $contentDataHashref->{body}{storage}{representation} = $contentDataHashref->{storageRepresentation};
        delete $contentDataHashref->{storageRepresentation};
    };

    # set default for some necessary values
    $contentDataHashref->{body}{storage}{representation} //= 'storage';
    $contentDataHashref->{type} //= 'page';

    my $jsonContentData = JSON->new->utf8->encode( $contentDataHashref );
    $self->{restClient}->PUT( '/rest/api/content/'.$id, $jsonContentData );
    confess('updateContent() returned unsuccessfully') unless $self->{restClient}->responseCode < 300;
    return decode_json($self->{restClient}->responseContent());
}

=head2 getAttachments

TODO

=cut

sub getAttachments {
    my $self = shift;
    my $id = shift;
    my $filename = shift;

    my $path = '/rest/api/content/'.$id.'/child/attachment?expand=version';
    $path .= "&filename=$filename" if defined $filename;

    $self->{restClient}->GET( $path );
    confess('getAttachments() returned unsuccessfully') unless $self->{restClient}->responseCode < 300;
    return decode_json($self->{restClient}->responseContent());
}

=head2 uploadAttachment

TODO

=cut

sub uploadAttachment {
    my $self = shift;
    my $id = shift;
    my $filename = shift;

    # TODO
    #    my $filedata = read_file($filename);
    #    $self->{restClient}->POST('/rest/api/content/'.$id.'/child/attachment', { Content_Type => 'form-data', Content => [ 'file' => [ $filename] ],  'X-Atlassian-Token' => 'nocheck'});
    use LWP::UserAgent;
    use HTTP::Request::Common;

    # FIXME: Import CA certificate
    $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;

    my $ua = LWP::UserAgent->new();
    $ua->ssl_opts( verify_hostname => 0 );
    IO::Socket::SSL::set_ctx_defaults(
        SSL_verifycn_scheme => 'www',
        SSL_verify_mode     => 0,
    );
    my $cwd = getcwd;
    chdir dirname($filename);
    my $response = $ua->request( POST('https://mam-confluence.1and1.com//rest/api/content/'.$id.'/child/attachment',
            (
                'Content_Type'      => 'form-data',
                'Content'           => [ 'file' => [ escape_filename(basename($filename)) ] ],
                'X-Atlassian-Token' => 'no-check',
                'Authorization'     => 'Basic '.encode_base64($self->{username}.':'.$self->{password}),
            )) );
    #confess('uploadAttachment() returned unsuccessfully') unless $self->{restClient}->responseCode < 300;
    #return decode_json($self->{restClient}->responseContent());
    chdir $cwd;
    return 1;
}

=head2 updateAttachmentData

TODO
curl -D- -u admin:admin -X POST -H "X-Atlassian-Token: nocheck" -F "file=@myfile.txt" -F "minorEdit=true" -F "comment=This is my File" http://myhost//rest/api/content/123/child/attachment
curl -D- -u admin:admin -X POST -H "X-Atlassian-Token: nocheck" -F "file=@myfile.txt" -F "minorEdit=true" -F "comment=This is my updated File" http://myhost//rest/api/content/123/child/attachment/456/data

=cut

sub updateAttachmentData {
    my $self = shift;
    my $id = shift;
    my $attachmentId = shift;
    my $filename = shift;
    my $minorEdit = shift() ? 'true' : 'false';
    print Dumper $minorEdit;

    # TODO
    #    my $filedata = read_file($filename);
    #    $self->{restClient}->POST('/rest/api/content/'.$id.'/child/attachment', { Content_Type => 'form-data', Content => [ 'file' => [ $filename] ],  'X-Atlassian-Token' => 'nocheck'});
    use LWP::UserAgent;
    use HTTP::Request::Common;

    # FIXME: Import CA certificate
    $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;

    my $ua = LWP::UserAgent->new();
    $ua->ssl_opts( verify_hostname => 0 );
    IO::Socket::SSL::set_ctx_defaults(
        SSL_verifycn_scheme => 'www',
        SSL_verify_mode     => 0,
    );
    my $response = $ua->request( POST(
            'https://mam-confluence.1and1.com//rest/api/content/'.$id.'/child/attachment/'.$attachmentId.'/data',
            (
                'Content_Type'      => 'form-data',
                'Content'           => { 'file' => [ $filename ], minorEdit => $minorEdit },
                'X-Atlassian-Token' => 'no-check',
                'Authorization'     => 'Basic '.encode_base64($self->{username}.':'.$self->{password}),
            )) );
    #confess('updateAttachmentData() returned unsuccessfully') unless $self->{restClient}->responseCode 300;
    #return decode_json($self->{restClient}->responseContent());
    return 1;
}

=head2 deleteContent

TODO

=cut

sub deleteContent {
    my $self = shift;
    my $id = shift;

    $self->{restClient}->DELETE( '/rest/api/content/'.$id );
    # REST call returns nothing, so we neither
    return undef;
}
# TODO: convertView(), convertStorage(), deleteContent()


# convienience wrapper for joining of arrays
sub _convertHashToParamUrl {
    my $self = shift;
    my $paramHash = shift;
    my $paramCount;
    my $paramUrl;

    warn( "Convert from:\n ".Dumper($paramHash) ) if $self->{debug};
    while ( my ($key, $value) = each %$paramHash ) {
        $paramUrl .= ( $paramCount++ ? '&' : '?' ).uri_encode($key, { encode_reserved => 1 } ).'=';

        if (ref $value eq 'ARRAY') {
            $paramUrl .= join( ',', map { uri_encode $_, { encode_reserved => 1 } } @$value );
        } else {
            $paramUrl .= uri_encode( $value, { encode_reserved => 1 } );
        }
    }
    warn( "Convert to:\n ".Dumper($paramUrl) ) if $self->{debug};

    return $paramUrl;
}


=head1 AUTHOR

Sebastian Heil, C<< <mail+perl at sebastianheil.de> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-confluence-client-rest at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=Confluence-Client-REST>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.

=head1 TODO

- better error handling
- return $id

see https://bitbucket.org/selberg/confluence-rest-example-from-perl for another implementation



=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Confluence::Client::REST


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=Confluence-Client-REST>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Confluence-Client-REST>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/Confluence-Client-REST>

=item * Search CPAN

L<http://search.cpan.org/dist/Confluence-Client-REST/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2016 Sebastian Heil.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut

1; # End of Confluence::Client::REST
