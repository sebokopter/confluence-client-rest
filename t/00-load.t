#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'Confluence::Client::REST' ) || print "Bail out!\n";
}

diag( "Testing Confluence::Client::REST $Confluence::Client::REST::VERSION, Perl $], $^X" );
