#!/usr/bin/env perl

use strict;
use warnings;
use lib '../lib';
use lib 'lib';
use lib 't';
use Test::More 'no_plan';
use Confluence::Client::REST;
use MockServer;

my $pid = MockServer::run();
my $confluence = Confluence::Client::REST->new({url => 'http://localhost:3000', username => "1234567890", password => "foobar", debug => 1});
use Data::Dumper;
#print Dumper($confluence->getContent());
my $output = $confluence->getContent();
ok(scalar @{$output->{results}} == 100, "retrieved all pages");
#print Dumper($confluence->getContent('1803174'));
#ok($confluence->getContent('1803174'));
#ok($confluence->getContent(1803174));
#ok($confluence->_getContent('1803174', { expand => 'children.page' } ));
#ok($confluence->getContent(1803174, qw(children.page)));
#
kill 9, $pid;
