package MockServer;

# TODO: delete git history to remove sensible Confluence data

use Dancer2;
use Dancer2::Plugin::HTTP::Auth::Extensible;
use File::Slurper qw(read_binary);
use HTTP::Server::Simple::PSGI;
use JSON ();

my $defaultHost = "localhost";
my $defaultPort = 3000;

set serializer => 'JSON';

set plugins => {
  'HTTP::Auth::Extensible' => {
    realms => {
      realm_one => {
        scheme => "Basic",
        provider => "Config",
        users => [
          { user => "1234567890",
            pass => "foobar",
          },
        ]
      },
    },
  }
};

set log => 'info';

get '/rest/api/content' => http_require_authentication sub {
  my $filename;
  my $content;
  if ( defined params->{start} && params->{start} != 0 ) {
    $filename = 'MockFiles/contentStart'.params->{start} 
  } else {
    $filename = 'MockFiles/content';
  }
  $content = JSON->new->utf8->decode(read_binary($filename)) ;
  return $content;
};

get '/rest/api/content/1803174' => http_require_authentication sub {
  return JSON->new->utf8->decode(read_binary('MockFiles/content1803174'));
};

get '/rest/api/content/1803174' => http_require_authentication sub {
  return JSON->new->utf8->decode(read_binary('MockFiles/content1803174'));
};

get '/rest/api/content/:id' => http_require_authentication sub {
  return JSON->new->utf8->decode(read_binary('MockFiles/noContentFound'));
};


sub run {
  my $host = shift // $defaultHost;
  my $port = shift // $defaultPort;
  my $server = HTTP::Server::Simple::PSGI->new($port);
  $server->host($host);
  $server->app(to_app);
  my $pid = $server->background;
  return $pid;
}

1;
